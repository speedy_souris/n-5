#coding:utf-8
"""
EXERCICE PYTHON #5
[Révision : fonctions, modularité]

> Concevoir un module de calcul de périmètre, aire et volume de plusieurs formes géométriques :
    - votre module sera un fichier nommé "geoshape.py".
    - vous devrez définir 6 fonction -> périmètre d'un carré
                                        aire d'un carré
                                        volume d'un cube
                                        périmètre d'un cercle
                                        aire d'un cercle
                                        volume d'une sphère.
    - prévoir l'exécution du module en indépendant qui effectura le test de toutes vos fonctions.
    - la valeur de Pi sera définie dans le module avec  pour valeur 3.14 (pour plus de facilité).
    - les valeurs et résultats seront en flottant - 2 chiffres après la virgule -> print("%.2f" % result)

> Idées d'évolution :
    - vous pourrez ajouter de nombreuses autres fonctions à votre module qui pourra prendre en charge une multitude d'autres formes
      (rectangle, triangle, losange, pyramide, cône, cylindre, ...)
    - prendre en charge une certaine fourchette de valeurs seulement (pour le côté d'un carré, le rayon d'un cercle, ...)
    - pour celles et ceux ayant abordé tkinker/pygame, on pourra élaborer un programme 2D pour apporter un côté plus visuel.
"""

from geometric import geoshape

#---------------------------
# Calculs pour le carré/cube
#---------------------------
square_length = 2








#---------------------------------
# Calculs pour le cercle/la sphère
#---------------------------------
circle_radius = 15

