#coding:utf-8
"""
    Module de calcul géométrique

> 5 fonctions de calcul sur un périmètre :
        - carré     -> square_perimeter(length)
        - cercle    -> circle_perimeter(radius)
        - rectangle -> rectangle_perimeter(lenght, width)
        - losange   -> diamond_perimeter(length)
        - triangle  -> triangle_perimeter(triangle_base, triangle_side1, triangle_side2)

> 5 fonctions de calcul sur un aire :
        - carré     -> square_area(length)
        - cercle    -> circle_area(radius, pi)
        - rectangle -> rectangle_perimeter(lenght, width)
        - losange   -> diamond_area(length)
        - triangle  -> triangle_area(triangle_base, triangle_side1, triangle_side2)

> 2 fonctions de calcul sur un volume :
        - cube      -> cube_volume(length, pi)
        - sphére    -> sphere_volume(radius, pi)
"""

                    #----------
                    # Périmètre
                    #----------

pi = 3.14

# Carré
def square_perimeter(length):
    return length * 4

# Cercle
def circle_perimeter(radius):
    return 2 * pi * radius

# Rectangle
def rectangle_perimeter(lenght, width):
    return (lenght + width) * 2

# Losange
def diamond_perimeter(length):
    return length * 4

# Triangle
def triangle_perimeter(triangle_base, triangle_side1, triangle_side2):
    return triangle_base + triangle_side1 + triangle_side2

                    #-----
                    # AIRE
                    #-----

# Carré
def square_area(length):
    return length**2

# Cercle
def circle_area(radius):
    return  pi * radius**2

# Rectangle
def rectangle_area(length, width):
    return length * width

# Losange
def diamond_area(length):
    return length**2

# Triangle
def triangle_area(triangle_base, triangle_side1, triangle_side2):
    s = triangle_perimeter(triangle_base, triangle_side1, triangle_side2)/2
    base = s - triangle_base
    side1 = s - triangle_side1
    side2 = s - triangle_side2
    t = s * base * side1 * side2
    return t**0.5

                    #-------
                    # Volume
                    #-------

# Cube
def cube_volume(length):
    return length**3

# Sphère
def sphere_volume(radius):
    return 4/3*pi*radius**3

if __name__ == '__main__':
    print(f"\npérimètre du carré : {square_perimeter(5)}\n")
    print("périmètre du cercle : %.2f" %circle_perimeter(12))
    print(f"\nAire du carré : {square_area(5)}")
    print("\nAire du cercle : %.2f" %circle_area(12))
    print(f"\nVolume du cube : {cube_volume(5)}")
    print("\nVolume de la sphère : %.3f" %sphere_volume(1.50geometry/))
    print(f"\nPérimètre du rectangle : {rectangle_perimeter(15,5)}")
    print(f"\nAire du rectangle : {rectangle_area(15,5)}")
    print(f"\npérimètre du losange : {diamond_perimeter(14)}")
    print(f"\nAire du losange : {diamond_area(14)}")
    print(f"\npérimètre du trianlge : {triangle_perimeter(17, 24, 9)}")
    print("\nAire du trianlge : %.2f\n" %triangle_area(17, 24, 9))
