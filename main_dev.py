#coding:utf-8
"""
EXERCICE PYTHON #5
[Révision : fonctions, modularité]

> Concevoir un module de calcul de périmètre, aire et volume de plusieurs formes géométriques :
    - votre module sera un fichier nommé "geoshape.py".
    - vous devrez définir 6 fonction -> périmètre d'un carré
                                        aire d'un carré
                                        volume d'un cube
                                        périmètre d'un cercle
                                        aire d'un cercle
                                        volume d'une sphère.
    - prévoir l'exécution du module en indépendant qui effectura le test de toutes vos fonctions.
    - la valeur de Pi sera définie dans le module avec  pour valeur 3.14 (pour plus de facilité).
    - les valeurs et résultats seront en flottant - 2 chiffres après la virgule -> print("%.2f" % result)

> Idées d'évolution :
    - vous pourrez ajouter de nombreuses autres fonctions à votre module qui pourra prendre en charge une multitude d'autres formes
      (rectangle, triangle, losange, pyramide, cône, cylindre, ...)
    - prendre en charge une certaine fourchette de valeurs seulement (pour le côté d'un carré, le rayon d'un cercle, ...)
    - pour celles et ceux ayant abordé tkinker/pygame, on pourra élaborer un programme 2D pour apporter un côté plus visuel.
"""

from geometry import geoshape

#----------------------------------------
# Calculs pour le carré/rectangle et cube
#----------------------------------------
square_length = 2
rectangle_width = square_length
rectangle_length = square_length * 3
diamond_length = 11
triangle_base = 17
triangle_side1 = 24
triangle_side2 = 9


print(f"\nPérimètre du carré : {geoshape.square_perimeter(square_length)}")

print(f"\nAire du carré : {geoshape.square_area(square_length)}")

print(f"\nVolume du cube : {geoshape.cube_volume(square_length)}")

print(f"\nPérimètre du rectangle : {geoshape.rectangle_perimeter(rectangle_length, rectangle_width)}")

print(f"\nAire du rectangle : {geoshape.rectangle_area(rectangle_length, rectangle_width)}")

print(f"\nPérimètre du losange : {geoshape.diamond_perimeter(diamond_length)}")

print(f"\nAire du losange : {geoshape.diamond_area(diamond_length)}")

print(f"\nPérimètre du triangle : {geoshape.triangle_perimeter(triangle_base, triangle_side1, triangle_side2)}")

print("\nAire du triangle : %.2f" %geoshape.triangle_area(triangle_base, triangle_side1, triangle_side2))

#---------------------------------
# Calculs pour le cercle/la sphère
#---------------------------------
circle_radius = 15

print("\npérimètre du cercle : %.2f" %geoshape.circle_perimeter(circle_radius))

print("\nAire du cercle : %.2f" %geoshape.circle_area(circle_radius))

print("\nVolume de la sphère : %.2f\n" %geoshape.sphere_volume(circle_radius))
